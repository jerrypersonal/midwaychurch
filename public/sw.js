(function () {

  var CACHE_NAME = 'mpcf-cache-v1';
  var urlsToCache = [
    '/',
    'index.html',
    'about.html',
    'contact.html',
    'messages.html',
    'notices.html',
    'testimony.html',
    'css/materialize.min.css',
    'css/timeliner.min.css',
    'js/index.js',
    'js/about.js',
    'js/lib.js',
    'js/message.js',
    'js/jquery-2.1.1.min.js',
    'js/materialize.min.js',
    'js/notices.js',
    'js/tabletop.min.js',
    'js/timeliner.min.js',
    'js/vue.min.js',
    'js/moment.min.js',
    'images/follow_me.jpg',
    'images/he_leads_me.jpg',
    'images/logo.png',
    'images/mp_abundant.jpg',
    'images/mp_other_side.jpg',
    'images/MPChapel.JPG'
  ];

  self.addEventListener('install', function(event) {
    // Perform install steps
    event.waitUntil(
      caches.open(CACHE_NAME)
      .then(function(cache) {
        console.log('Opened cache');
        return cache.addAll(urlsToCache);
      })
    );
  });

  self.addEventListener('fetch', function(event) {
    event.respondWith(
      caches.match(event.request)
      .then(function(response) {
        // Cache hit - return response
        if (response) {
          return response;
        }

        // IMPORTANT: Clone the request. A request is a stream and
        // can only be consumed once. Since we are consuming this
        // once by cache and once by the browser for fetch, we need
        // to clone the response.
        var fetchRequest = event.request.clone();

        return fetch(fetchRequest).then(
          function(response) {
            // Check if we received a valid response
            if(!response || response.status !== 200 || response.type !== 'basic') {
              return response;
            }

            // IMPORTANT: Clone the response. A response is a stream
            // and because we want the browser to consume the response
            // as well as the cache consuming the response, we need
            // to clone it so we have two streams.
            var responseToCache = response.clone();

            caches.open(CACHE_NAME)
            .then(function(cache) {
              cache.put(event.request, responseToCache);
            });

            return response;
          }
        );
      })
    );
  });

})();
