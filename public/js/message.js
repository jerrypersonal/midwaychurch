(function($){
  $(function(){
    $('.button-collapse').sideNav();
    $('.parallax').parallax();
    $('.slider').slider({full_width: true});
    // createStoryJS({
    //     type:       'timeline',
    //     width:      '100%',
    //     height:     '600',
    //     source:     'https://docs.google.com/spreadsheets/d/1O6xlFIfsxz_f9uFQcGN6Rk51T6vugas1MCIcpYI-7sM/pubhtml',
    //     embed_id:   'my-timeline',
    //     start_at_end: true
    // });
    var additionalOptions = {
      start_at_end: true
    }
    timeline = new TL.Timeline('timeline-embed',
    'https://docs.google.com/spreadsheets/d/10Sd9hwkFTjaTXuLacieST50rHDYLRmUEyP8oJ11MM8I/pubhtml', additionalOptions);
    }); // end of document ready
})(jQuery); // end of jQuery name space
