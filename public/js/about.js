(function($){
  $(function(){
    $('.button-collapse').sideNav();
    $('.parallax').parallax();
    $('.slider').slider({full_width: true});
    // createStoryJS({
    //     type:       'timeline',
    //     width:      '100%',
    //     height:     '600',
    //     source:     'https://docs.google.com/spreadsheets/d/11l04rsAAn-D0gdq_zz7FxAY2Sr2gfbS_myBC37xAWOk/pubhtml',
    //     embed_id:   'my-timeline'
    // });
    }); // end of document ready
  })(jQuery); // end of jQuery name space
