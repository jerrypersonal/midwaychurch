(function($){
  $(function(){
    $('.button-collapse').sideNav();
    $('.parallax').parallax();
    Tabletop.init( { key: 'https://docs.google.com/spreadsheets/d/1ZAJPG3hlCw_GCTMFsyA41HLL5XSef9iWUDvAiLo1rUQ/pubhtml',
                     callback: function(data, tabletop) {
                         console.log(data)
                     },
                     simpleSheet: true } )

  }); // end of document ready
})(jQuery); // end of jQuery name space
