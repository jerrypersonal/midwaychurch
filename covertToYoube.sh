#!/bin/bash

# convert audio and jpg to video with youtube format

function youtube {
	ffmpeg -loop 1 -framerate 1 -i logo.png -i $1 -c:v libx264 -preset medium -tune stillimage -crf 18 -c:a copy -shortest -pix_fmt yuv420p $(dirname $file)/$(basename $file .wma).mkv
}

# convert all .wma to youtube video
function convertDir {
	for file in $1/*;do
		youtube $file
	done
}

for dir in Teaching_recorders/*;do
	convertDir $dir
done
